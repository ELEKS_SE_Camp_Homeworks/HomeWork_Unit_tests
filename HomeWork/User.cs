﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    public class User
    {
        public string Username { get; set; }
        public Guid Id { get; }
        public string Email { get; set; }
        public bool IsPremium { get; set; }
        public List<Mail> MailBox { get; }

        public User(string username, string email, bool isPremium)
        {
            Username = username;
            Id = Guid.NewGuid();
            Email = email;
            IsPremium = isPremium;
            MailBox = new List<Mail>();
        }
    }
}
