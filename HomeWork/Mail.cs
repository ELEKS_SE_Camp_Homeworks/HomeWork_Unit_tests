﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    public class Mail
    {
        public string Content { get; set; }
        public Guid ReсeiverId { get; }
        public Guid SenderId { get; }
        public DateTime Date { get; set; }

        public Mail(string content, Guid reсeiverId, Guid senderId)
        {
            Content = content;
            ReсeiverId = reсeiverId;
            SenderId = senderId;
            Date = DateTime.Today;
        }
    }
}
