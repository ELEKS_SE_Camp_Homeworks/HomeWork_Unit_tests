﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    public interface IViewRepository
    {
        //Adds new user
        void AddUser(User user);

        // Returns all users in the repository
        IEnumerable<User> GetUsers();

        // Returns all premium users in the repository
        IEnumerable<User> GetPremiumUsers();
    }

    public class UserRepositoryFakе : IViewRepository
    {
        private readonly List<User> _usersList = new List<User>();

        public void AddUser(User user)
        {
            if (user == null) throw new ArgumentNullException();
            if (_usersList.Any(it => it.Email.Equals(user.Email)))
                throw new Exception("This email addres is already occupied by another user");
            _usersList.Add(user);
        }

        public IEnumerable<User> GetUsers()
        {
            return _usersList;
        }

        public IEnumerable<User> GetPremiumUsers()
        {
            return _usersList.Where(user => user.IsPremium).ToList();
        }
    }
}
