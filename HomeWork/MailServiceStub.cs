﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    public interface IMailService
    {
        // Sends the mail to target user
        void SendMail(User to, User from,string mailText);
    }

    public class MailServiceStub : IMailService
    {
        public void SendMail(User receiverUser,User senderUser,string mailText)
        {
            if(receiverUser == null || senderUser == null) throw new ArgumentNullException();
            if (receiverUser.Id.CompareTo(senderUser.Id) != 0)
                receiverUser.MailBox.Add(new Mail(mailText, receiverUser.Id, senderUser.Id));
        }
    }
}
