﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HomeWork
{
    public interface IUserMailNotifier
    {
        void MailPremiumUsers(User sender,string mailText);
    }

    public class UserMailNotifier : IUserMailNotifier
    {
        private IMailService _mailService;
        private IViewRepository _viewRepository;

        public UserMailNotifier(IMailService mailService, IViewRepository viewRepository)
        {
            _mailService = mailService;
            _viewRepository = viewRepository;
        }

        public void MailPremiumUsers(User sender,string mailText)
        {
            foreach (var premUser in _viewRepository.GetPremiumUsers())
            {
                _mailService.SendMail(premUser, sender,mailText);
            }
        }

    }
}
