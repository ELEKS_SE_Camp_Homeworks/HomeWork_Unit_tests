﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace HomeWork.UnitTests
{
    [TestFixture]
    class MailServiceStubTests
    {
        [Test]
        public void Should_Throw_ArgumentNullException_When_Add_Null_Sender_Or_Receiver()
        {
            try
            {
                IMailService mailService = new MailServiceStub();
                var testUser = new User("Oleh", "oleh.email@gmail.com", true);
                mailService.SendMail(null,null,"Hello");
                Assert.Fail("An exception should have been thrown");
            }
            catch (ArgumentNullException e)
            {
                Console.WriteLine(e.Message);
            }
        }

        [Test]
        public void Message_Will_Not_Be_Sent_To_Yourself()
        {
            IMailService mailService = new MailServiceStub();
            var testUser = new User("Oleh", "oleh.email@gmail.com", true);
            int expectedMailCount = 0;

            mailService.SendMail(testUser, testUser, "Hello");

            Assert.AreEqual(expectedMailCount,testUser.MailBox.Count);
        }
    }
}
