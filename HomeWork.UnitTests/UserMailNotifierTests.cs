﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace HomeWork.UnitTests
{
    [TestFixture]
    class UserMailNotifierTests
    {
        [Test]
        public void Message_Should_Send_To_All_Premium_Users()
        {
            var premiumUser1 = new User("User1","user1@gmail.com",true);
            var premiumUser2 = new User("User1","user2@gmail.com",true);
            var premiumUser3 = new User("User1","user3@gmail.com",true);
            int expectedMailCount = 2;
            IViewRepository viewRepository = new UserRepositoryFakе();
            viewRepository.AddUser(premiumUser1);
            viewRepository.AddUser(premiumUser2);
            viewRepository.AddUser(premiumUser3);
            IMailService mailService = new MailServiceStub();
            IUserMailNotifier mailNotifier = new UserMailNotifier(mailService,viewRepository);

            mailNotifier.MailPremiumUsers(new User("sender","sende@gmail.com",false),"Hello");
            mailNotifier.MailPremiumUsers(new User("sender","sende@gmail.com",false),"Hello again");

            foreach (var premUsr in viewRepository.GetPremiumUsers())
            {
                Assert.AreEqual(expectedMailCount,premUsr.MailBox.Count);
            }

        }
    }
}
