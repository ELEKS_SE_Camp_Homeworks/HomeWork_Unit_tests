﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace HomeWork.UnitTests
{
    [TestFixture]
    class UserRepositoryFakeTests
    {
        [Test]
        public void Should_Throw_ArgumentNullException_When_Add_NullRefferenceUser()
        {
            //Arrange
            IViewRepository viewRepository = new UserRepositoryFakе();

            //Act/Assert
            Assert.Throws(typeof(ArgumentNullException), () => viewRepository.AddUser(null));
        }

        [Test]
        public void Should_Throw_Exception_When_Add_User_With_AlreadyOccupied_Email()
        {
            //Arrange
            var firstTestUser = new User("TestName1","example@example.com",true);
            var secondTestUser = new User("TestName2","example@example.com",true);
            IViewRepository viewRepository = new UserRepositoryFakе();

            //Act
            viewRepository.AddUser(firstTestUser);

            //Assert
            Assert.Throws<Exception>(() => viewRepository.AddUser(secondTestUser), "This email addres is already occupied by another user");
        }

        [Test]
        public void When_Add_User_To_Repository_Should_Add_To_UsersList()
        {
            //Arrange
            var testUser = new User("TestName", "example@example.com", true);
            IViewRepository viewRepository = new UserRepositoryFakе();
            int expectedCount = 1;

            //Act
            viewRepository.AddUser(testUser);

            //Assert
            Assert.AreEqual(expectedCount,viewRepository.GetUsers().Count());
        }

        [Test]
        public void After_Adding_User_Repository_Should_Contain_Him()
        {
            //Arrange
            var testUser = new User("TestName", "example@example.com", true);
            IViewRepository viewRepository = new UserRepositoryFakе();

            //Act
            viewRepository.AddUser(testUser);

            //Assert
            Assert.Contains(testUser,viewRepository.GetUsers().ToList());
        }
    }
}
